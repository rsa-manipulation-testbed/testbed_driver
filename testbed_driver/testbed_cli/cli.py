#!/usr/bin/env python3

import rospy
import click

from testbed_msgs.srv import AxisEnable, SetAxisEncoder


@click.group()
def cli():
    rospy.init_node("testbed_cli", anonymous=True)
    pass


@cli.command()
def enable():
    rospy.loginfo("Enabling all axes")
    srvpxy = rospy.ServiceProxy("/testbed/enable_all", AxisEnable)

    resp = srvpxy(True)
    if not resp:
        rospy.loginfo("Service request failed")


def disable_all():
    srvpxy = rospy.ServiceProxy("/testbed/enable_all", AxisEnable)

    resp = srvpxy(False)
    if not resp:
        rospy.loginfo("Service request failed")


@cli.command()
def disable():
    rospy.loginfo("Disabling all axes")
    disable_all()


# ~~~~ ~~~~


def zero_axis(axis):
    rospy.loginfo("Zeroing %s" % axis)
    rospy.wait_for_service("/testbed/%s/set_encoder_value" % axis)
    srvcall = rospy.ServiceProxy("/testbed/%s/set_encoder_value" % axis, SetAxisEncoder)

    # Ensure all of the controllers are loaded
    #  Todo add error handling
    result = srvcall(0)
    rospy.logwarn(result)


@cli.command()
@click.argument("axes", required=True, nargs=-1)
def zero(axes):
    valid_axes = ["yaw", "zmotor", "corexy"]

    for axis in axes:
        if axis not in valid_axes:
            rospy.logfatal("Don't understand the axis %s" % axis)
            return

    rospy.loginfo(axes)

    # Validate axes
    if len(axes) == 0:
        return

    rospy.loginfo("Disabling all axes")
    disable_all()

    for axis in axes:
        if axis == "corexy":
            zero_axis("corexy_a")
            zero_axis("corexy_b")
        else:
            zero_axis(axis)
