// Copyright 2021 University of Washington

#include "testbed_driver/testbed.h"

#include <ros/console.h>

#include <string>

#include "clearpath_sc_ros/clearpath_motor_hw.h"
#include "joint_limits_interface/joint_limits_rosparam.h"
#include "motor_hw_base/sim_motor_hw.h"
#include "testbed_msgs/ActuatorFloatStamped.h"
#include "testbed_msgs/ActuatorState.h"
#include "testbed_msgs/EncoderValue.h"
#include "testbed_msgs/JointFloatStamped.h"
#include "testbed_msgs/TestbedState.h"

namespace testbed_driver {

using hardware_interface::ControllerInfo;
using namespace transmission_interface;
using clearpath_sc_ros::ClearpathMotorHw;

const float Testbed::DiagnosticUpdateTimerPeriod = 0.1;
const double Testbed::ControlLoopTimerPeriod = (1.0 / 20);

// Just as a reminder to myself, SimpleTransmissions are implemented as
//   actuator = ratio * joint
//
// So for example for corexy:
//   rotational actuator = linear travel * 1 / radius of pulley
//
// For the lead screw
//   rotational actuator = linear z travel * (2Pi / travel per revolution)

// Current pulley is 5cm radius
// This can be overriden by a rosparam
const float Testbed::DefaultCoreXyPulleyRadius = 0.02228;

// Current lead screw is 0.1" per turn
// With a 10:1 gearbox in-line
const float Testbed::DefaultZAxisScrewTravel = -0.000254;

// Gear ratio on Yaw.  This is (yaw gear teeth / motor gear teeth)
const float Testbed::DefaultYawGearRatio = -5;

const std::array<std::string, Testbed::NumJoints> Testbed::JointNames = {
    "corexy_x", "corexy_y", "zaxis", "yaw"};
const std::array<std::string, Testbed::NumActuators> Testbed::ActuatorNames = {
    "corexy_a", "corexy_b", "zmotor", "yawmotor"};

Testbed::Testbed()
    : yaw_control_mode_(Control_Idle),
      coreXY_control_mode_(Control_Idle),
      zaxis_control_mode_(Control_Idle),
      coreXY_trans_(
          {1 / DefaultCoreXyPulleyRadius, 1 / DefaultCoreXyPulleyRadius}),
      zaxis_trans_((2 * M_PI) / DefaultZAxisScrewTravel),
      yaw_trans_(DefaultYawGearRatio) {}

Testbed::~Testbed() {
  if (_yaw) _yaw->disable();
  if (_coreXyA) _coreXyA->disable();
  if (_coreXyB) _coreXyB->disable();
  if (_zAxis) _zAxis->disable();
}

bool Testbed::init(ros::NodeHandle &nh, ros::NodeHandle &pnh) {
  enable_all_server_ =
      pnh.advertiseService("enable_all", &Testbed::enableSrvCallback, this);

  testbed_state_pub_ = nh.advertise<testbed_msgs::TestbedState>("state", 10);

  joint_command_input_pub_ = nh.advertise<testbed_msgs::JointFloatStamped>(
      "debug/joint/velocity_input", 10);
  joint_command_limited_pub_ = nh.advertise<testbed_msgs::JointFloatStamped>(
      "debug/joint/velocity_limited", 10);
  actuator_command_pub_ = nh.advertise<testbed_msgs::ActuatorFloatStamped>(
      "debug/actuator/velocity_cmd", 10);

  diagnostic_updater_.setHardwareID("testbed");
  nh.createTimer(
      ros::Duration(Testbed::DiagnosticUpdateTimerPeriod),
      [&](const ros::TimerEvent &event) { diagnostic_updater_.update(); });

  ROS_WARN("Starting init");

  bool is_sim;
  pnh.getParam("sim", is_sim);

  if (is_sim) {
    ROS_WARN("** SIMULATION MODE **");
    _yaw = std::make_shared<SimMotorHw>(ActuatorNames[YawMotor], nh);
    _coreXyA = std::make_shared<SimMotorHw>(ActuatorNames[CoreXyA], nh);
    _coreXyB = std::make_shared<SimMotorHw>(ActuatorNames[CoreXyB], nh);
    _zAxis = std::make_shared<SimMotorHw>(ActuatorNames[ZMotor], nh);

  } else {
    bool enableCoreXy = false, enableYaw = false, enableZAxis = false;
    int yaw_sn = 0, zaxis_sn = 0, corexy_a_sn = 0, corexy_b_sn = 0;

    pnh.getParam("enable_yaw", enableYaw);
    pnh.getParam("yaw_sn", yaw_sn);

    pnh.getParam("enable_zaxis", enableZAxis);
    pnh.getParam("zaxis_sn", zaxis_sn);

    pnh.getParam("enable_corexy", enableCoreXy);
    pnh.getParam("corexy_a_sn", corexy_a_sn);
    pnh.getParam("corexy_b_sn", corexy_b_sn);

    try {
      manager_.initialize();

      // Disable global shutdown for all motors (prevents issues with estop
      // input on SC-HUB)
      manager_.setGlobalShutdown(false);

      ROS_INFO_STREAM("Found " << manager_.axes().size() << " nodes on "
                               << manager_.numPorts() << " ports");

      for (auto const &axis : manager_.axes()) {
        sFnd::IInfo &info(axis->getInfo());

        ROS_INFO_STREAM("  " << info.Model.Value() << " : s/n "
                             << info.SerialNumber.Value() << " : f/w "
                             << info.FirmwareVersion.Value());

        int sn = int(info.SerialNumber.Value());
        if (enableYaw && (sn == yaw_sn)) {
          _yaw = std::make_shared<ClearpathMotorHw>(ActuatorNames[YawMotor], nh,
                                                    axis);
        } else if (enableZAxis && (sn == zaxis_sn)) {
          _zAxis = std::make_shared<ClearpathMotorHw>(ActuatorNames[ZMotor], nh,
                                                      axis);
        } else if (enableCoreXy) {
          if (sn == corexy_a_sn) {
            _coreXyA = std::make_shared<ClearpathMotorHw>(
                ActuatorNames[CoreXyA], nh, axis);
          } else if (sn == corexy_b_sn)
            _coreXyB = std::make_shared<ClearpathMotorHw>(
                ActuatorNames[CoreXyB], nh, axis);
        }
      }

    } catch (sFnd::mnErr &theErr) {
      ROS_ERROR("Caught error");
      printf("Caught error: addr=%d, err=0x%08x\nmsg=%s\n", theErr.TheAddr,
             theErr.ErrorCode, theErr.ErrorMsg);

      return false;
    }

    if (enableYaw && !_yaw) {
      ROS_FATAL("Did not find Yaw motor");
      return false;
    }

    if (enableZAxis && !_zAxis) {
      ROS_FATAL("Did not find ZAxis motor");
      return false;
    }

    if (enableCoreXy) {
      if (!_coreXyA) {
        ROS_FATAL("Did not find CoreXY A motor");
        return false;
      } else if (!_coreXyB) {
        ROS_FATAL("Did not find CoreXY B motor");
        return false;
      }

      float coreXyPulleyRadius = DefaultCoreXyPulleyRadius;
      nh.getParam("corexy_pulley_radius_m", coreXyPulleyRadius);

      ROS_INFO_STREAM("Using CoreXY pulley radius of "
                      << coreXyPulleyRadius * 1000 << " mm");

      coreXY_trans_.setActuatorReduction(1.0 / coreXyPulleyRadius);
    }
  }

  for (int i = 0; i < NumActuators; i++) {
    const std::string actuator_name = ActuatorNames[i];

    std::string offset_param_name("zero_offset/");
    offset_param_name += actuator_name;

    int offset_raw;
    if (pnh.getParam(offset_param_name, offset_raw)) {
      ROS_INFO_STREAM("Using offset " << offset_raw << " for actuator \""
                                      << actuator_name << "\"");
      actuator_offset_raw_[i] = offset_raw;
    } else {
      ROS_INFO_STREAM("No offset specified for actuator \""
                      << actuator_name << "\", using zero");
      actuator_offset_raw_[i] = 0;
    }
  }

  for (int i = 0; i < NumJoints; i++) {
    const std::string jointName = JointNames[i];

    // -- connect and register the ros_control joint state interface --
    hardware_interface::JointStateHandle stateHandle(
        jointName, &joint_position_.data()[i], &joint_velocity_.data()[i],
        &joint_current_.data()[i]);
    joint_state_interface_.registerHandle(stateHandle);

    hardware_interface::JointHandle velHandle(
        joint_state_interface_.getHandle(jointName),
        &joint_velocity_command_.data()[i]);
    joint_velocity_interface_.registerHandle(velHandle);

    // Add joint limits
    if (!joint_limits_interface::getJointLimits(jointName, nh, _limits)) {
      ROS_WARN_STREAM("Unable to find joint limits for " << jointName);
    }

    joint_limits_interface::VelocityJointSaturationHandle jointLimitsHandle(
        velHandle, _limits);
    velocity_joint_limit_interface_.registerHandle(jointLimitsHandle);
  }

  registerInterface(&joint_state_interface_);
  registerInterface(&joint_velocity_interface_);

  // Set up the CoreXY transmission
  // Map CoreXY states in both directions
  coreXY_trans_act_state_.position.push_back(&actuator_position_[CoreXyA]);
  coreXY_trans_act_state_.position.push_back(&actuator_position_[CoreXyB]);
  coreXY_trans_act_state_.velocity.push_back(&actuator_velocity_[CoreXyA]);
  coreXY_trans_act_state_.velocity.push_back(&actuator_velocity_[CoreXyB]);
  coreXY_trans_act_state_.effort.push_back(&actuator_current_[CoreXyA]);
  coreXY_trans_act_state_.effort.push_back(&actuator_current_[CoreXyB]);

  coreXY_trans_joint_state_.position.push_back(&joint_position_[CoreXyX]);
  coreXY_trans_joint_state_.position.push_back(&joint_position_[CoreXyY]);
  coreXY_trans_joint_state_.velocity.push_back(&joint_velocity_[CoreXyX]);
  coreXY_trans_joint_state_.velocity.push_back(&joint_velocity_[CoreXyY]);
  coreXY_trans_joint_state_.effort.push_back(&joint_current_[CoreXyX]);
  coreXY_trans_joint_state_.effort.push_back(&joint_current_[CoreXyY]);

  // Map CoreXY vel commands in both directions
  coreXY_trans_act_cmd_.velocity.push_back(
      &actuator_velocity_command_[CoreXyA]);
  coreXY_trans_act_cmd_.velocity.push_back(
      &actuator_velocity_command_[CoreXyB]);
  coreXY_trans_joint_cmd_.velocity.push_back(&joint_velocity_command_[CoreXyX]);
  coreXY_trans_joint_cmd_.velocity.push_back(&joint_velocity_command_[CoreXyY]);

  act_to_joint_state_.registerHandle(ActuatorToJointStateHandle(
      "corexy", &coreXY_trans_, coreXY_trans_act_state_,
      coreXY_trans_joint_state_));
  joint_to_act_vel_.registerHandle(JointToActuatorVelocityHandle(
      "corexy", &coreXY_trans_, coreXY_trans_act_cmd_,
      coreXY_trans_joint_cmd_));

  // Map yaw states and commands in both directions
  yaw_trans_act_state_.position.push_back(&actuator_position_[YawMotor]);
  yaw_trans_act_state_.velocity.push_back(&actuator_velocity_[YawMotor]);
  yaw_trans_act_state_.effort.push_back(&actuator_current_[YawMotor]);

  yaw_trans_joint_state_.position.push_back(&joint_position_[Yaw]);
  yaw_trans_joint_state_.velocity.push_back(&joint_velocity_[Yaw]);
  yaw_trans_joint_state_.effort.push_back(&joint_current_[Yaw]);

  yaw_trans_act_cmd_.velocity.push_back(&actuator_velocity_command_[YawMotor]);
  yaw_trans_joint_cmd_.velocity.push_back(&joint_velocity_command_[Yaw]);

  act_to_joint_state_.registerHandle(ActuatorToJointStateHandle(
      "yaw", &yaw_trans_, yaw_trans_act_state_, yaw_trans_joint_state_));
  joint_to_act_vel_.registerHandle(JointToActuatorVelocityHandle(
      "yaw", &yaw_trans_, yaw_trans_act_cmd_, yaw_trans_joint_cmd_));

  // Map ZAxis states and commands in both directions
  zaxis_trans_act_state_.position.push_back(&actuator_position_[ZMotor]);
  zaxis_trans_act_state_.velocity.push_back(&actuator_velocity_[ZMotor]);
  zaxis_trans_act_state_.effort.push_back(&actuator_current_[ZMotor]);

  zaxis_trans_joint_state_.position.push_back(&joint_position_[ZAxis]);
  zaxis_trans_joint_state_.velocity.push_back(&joint_velocity_[ZAxis]);
  zaxis_trans_joint_state_.effort.push_back(&joint_current_[ZAxis]);

  zaxis_trans_act_cmd_.velocity.push_back(&actuator_velocity_command_[ZMotor]);
  zaxis_trans_joint_cmd_.velocity.push_back(&joint_velocity_command_[ZAxis]);

  act_to_joint_state_.registerHandle(
      ActuatorToJointStateHandle("zaxis", &zaxis_trans_, zaxis_trans_act_state_,
                                 zaxis_trans_joint_state_));
  joint_to_act_vel_.registerHandle(JointToActuatorVelocityHandle(
      "zaxis", &zaxis_trans_, zaxis_trans_act_cmd_, zaxis_trans_joint_cmd_));

  // Create the controller manager
  controller_manager_.reset(
      new controller_manager::ControllerManager(this, nh));
  control_loop_timer_ = nh.createTimer(ros::Duration(ControlLoopTimerPeriod),
                                       &Testbed::update, this);

  return true;
}

void Testbed::update(const ros::TimerEvent &e) {
  elapsed_time_ = ros::Duration(e.current_real - e.last_real);
  read(ros::Time::now(), elapsed_time_);
  controller_manager_->update(ros::Time::now(), elapsed_time_);
  write(ros::Time::now(), elapsed_time_);
}

testbed_msgs::EncoderValue encoderValueToEncoderValue(
    const EncoderValue &value) {
  testbed_msgs::EncoderValue msg;

  msg.metric = value.metric();
  msg.raw = value.trueRaw();
  msg.offset = value.offsetRaw();
  msg.raw_to_metric = value.rawToMetric();

  return msg;
}

testbed_msgs::ActuatorState actuatorStateToJointState(
    const MotorState &motor_state) {
  testbed_msgs::ActuatorState msg;

  msg.position = encoderValueToEncoderValue(motor_state.position);
  msg.velocity = encoderValueToEncoderValue(motor_state.velocity);
  msg.current = encoderValueToEncoderValue(motor_state.current);
  return msg;
}

void Testbed::read(const ros::Time &current_time,
                   const ros::Duration &elapsed_time) {
  // Publish our custom TestbedState message as well
  testbed_msgs::TestbedState state_msg;
  state_msg.header.stamp = current_time;

  // Update motor, and wait for a response
  if (_yaw) {
    MotorState state = _yaw->state();
    state.position.setOffsetRaw(actuator_offset_raw_[YawMotor]);

    actuator_position_[YawMotor] = state.position.metric();
    actuator_velocity_[YawMotor] = state.velocity.metric();
    actuator_current_[YawMotor] = state.current.metric();

    state_msg.actuators.yaw = actuatorStateToJointState(state);
  } else {
    actuator_position_[YawMotor] = actuator_velocity_[YawMotor] =
        actuator_current_[YawMotor] = 0.0;
  }

  if (_coreXyA && _coreXyB) {
    {
      MotorState state = _coreXyA->state();
      state.position.setOffsetRaw(actuator_offset_raw_[CoreXyA]);

      actuator_position_[CoreXyA] = state.position.metric();
      actuator_velocity_[CoreXyA] = state.velocity.metric();
      actuator_current_[CoreXyA] = state.current.metric();

      state_msg.actuators.coreXyA = actuatorStateToJointState(state);
    }

    {
      MotorState state = _coreXyB->state();
      state.position.setOffsetRaw(actuator_offset_raw_[CoreXyB]);

      actuator_position_[CoreXyB] = state.position.metric();
      actuator_velocity_[CoreXyB] = state.velocity.metric();
      actuator_current_[CoreXyB] = state.current.metric();

      state_msg.actuators.coreXyB = actuatorStateToJointState(state);
    }
  } else {
    actuator_position_[CoreXyA] = actuator_velocity_[CoreXyA] =
        actuator_current_[CoreXyA] = 0.0;
    actuator_position_[CoreXyB] = actuator_velocity_[CoreXyB] =
        actuator_current_[CoreXyB] = 0.0;
  }

  if (_zAxis) {
    MotorState state = _zAxis->state();
    state.position.setOffsetRaw(actuator_offset_raw_[ZMotor]);

    actuator_position_[ZMotor] = state.position.metric();
    actuator_velocity_[ZMotor] = state.velocity.metric();
    actuator_current_[ZMotor] = state.current.metric();

    state_msg.actuators.z = actuatorStateToJointState(state);
  } else {
    actuator_position_[ZMotor] = actuator_velocity_[ZMotor] =
        actuator_current_[ZMotor] = 0.0;
  }

  act_to_joint_state_.propagate();

  state_msg.joints.yaw.position = joint_position_[Yaw];
  state_msg.joints.yaw.velocity = joint_velocity_[Yaw];
  state_msg.joints.yaw.current = joint_current_[Yaw];

  state_msg.joints.x.position = joint_position_[CoreXyX];
  state_msg.joints.x.velocity = joint_velocity_[CoreXyX];
  state_msg.joints.x.current = joint_current_[CoreXyX];

  state_msg.joints.y.position = joint_position_[CoreXyY];
  state_msg.joints.y.velocity = joint_velocity_[CoreXyY];
  state_msg.joints.y.current = joint_current_[CoreXyY];

  state_msg.joints.z.position = joint_position_[ZAxis];
  state_msg.joints.z.velocity = joint_velocity_[ZAxis];
  state_msg.joints.z.current = joint_current_[ZAxis];

  testbed_state_pub_.publish(state_msg);
}

void Testbed::write(const ros::Time &current_time,
                    const ros::Duration &elapsed_time) {
  testbed_msgs::JointFloatStamped joint_input_msg, joint_limited_msg;
  testbed_msgs::ActuatorFloatStamped actuator_cmd_msg;

  joint_input_msg.header.stamp = current_time;
  joint_input_msg.x = joint_velocity_command_[CoreXyX];
  joint_input_msg.y = joint_velocity_command_[CoreXyY];
  joint_input_msg.yaw = joint_velocity_command_[Yaw];
  joint_input_msg.z = joint_velocity_command_[ZAxis];

  joint_command_input_pub_.publish(joint_input_msg);

  velocity_joint_limit_interface_.enforceLimits(elapsed_time);

  joint_limited_msg.header = joint_input_msg.header;
  joint_limited_msg.x = joint_velocity_command_[CoreXyX];
  joint_limited_msg.y = joint_velocity_command_[CoreXyY];
  joint_limited_msg.yaw = joint_velocity_command_[Yaw];
  joint_limited_msg.z = joint_velocity_command_[ZAxis];
  joint_command_limited_pub_.publish(joint_limited_msg);

  joint_to_act_vel_.propagate();

  actuator_cmd_msg.header = joint_input_msg.header;
  actuator_cmd_msg.coreXyA = actuator_velocity_command_[CoreXyA];
  actuator_cmd_msg.coreXyB = actuator_velocity_command_[CoreXyB];
  actuator_cmd_msg.yaw = actuator_velocity_command_[YawMotor];
  actuator_cmd_msg.z = actuator_velocity_command_[ZMotor];
  actuator_command_pub_.publish(actuator_cmd_msg);

  if (_yaw && _yaw->isEnabled()) {
    if (yaw_control_mode_ == Control_Velocity) {
      ROS_DEBUG_STREAM("Setting yaw actuator velocity to "
                       << actuator_velocity_command_[YawMotor] << " rad/s");
      _yaw->setVelocity(actuator_velocity_command_[YawMotor],
                        ControlLoopTimerPeriod, true);
    } else {
      ROS_INFO_STREAM("Setting yaw actuator to idle");
      _yaw->stop();
    }
  }

  if (_coreXyA && _coreXyB) {
    if (_coreXyA->isEnabled() && _coreXyB->isEnabled()) {
      if (coreXY_control_mode_ == Control_Velocity) {
        ROS_DEBUG_STREAM("Setting coreXY actuator velocities to A ="
                         << actuator_velocity_command_[CoreXyA] << "; B = "
                         << actuator_velocity_command_[CoreXyB] << " rad/s");
        _coreXyA->setVelocity(actuator_velocity_command_[CoreXyA],
                              ControlLoopTimerPeriod, true);
        _coreXyB->setVelocity(actuator_velocity_command_[CoreXyB],
                              ControlLoopTimerPeriod, true);
      } else {
        ROS_INFO_STREAM("Setting coreXY actuators to idle");
        _coreXyA->stop();
        _coreXyB->stop();
      }
    }
  }

  if (_zAxis && _zAxis->isEnabled()) {
    if (zaxis_control_mode_ == Control_Velocity) {
      ROS_DEBUG_STREAM("Setting ZAxis velocity to "
                       << actuator_velocity_command_[ZMotor] << " rad/s");
      _zAxis->setVelocity(actuator_velocity_command_[ZMotor],
                          ControlLoopTimerPeriod, true);
    } else {
      ROS_INFO_STREAM("Setting ZAxis actuators to idle");
      _zAxis->stop();
    }
  }
}

void Testbed::doSwitch(const std::list<ControllerInfo> &start_list,
                       const std::list<ControllerInfo> &stop_list) {
  ROS_WARN_STREAM("In Testbed::doSwitch, starting "
                  << start_list.size() << ", stopping " << stop_list.size());

  for (auto const &c : stop_list) {
    ROS_WARN_STREAM("--- Stopping controller " << c.name << " : " << c.type);

    for (auto const &r : c.claimed_resources) {
      ROS_WARN_STREAM("---   Hardware resources : " << r.hardware_interface);
      for (auto const &resource : r.resources) {
        ROS_WARN_STREAM("---     Resource : " << resource);

        // Basically, if any controller stops for yaw, set yaw to Control_None
        // And similar for CoreXy

        if (resource == JointNames[Yaw]) {
          ROS_WARN_STREAM("Stopping controller " << c.name
                                                 << " for yaw, idling yaw");
          yaw_control_mode_ = Control_Idle;

        } else if (resource == JointNames[CoreXyX] ||
                   resource == JointNames[CoreXyY]) {
          ROS_WARN_STREAM("Stopping controller "
                          << c.name << " for CoreXY, idling both axes");
          coreXY_control_mode_ = Control_Idle;
        } else if (resource == JointNames[ZAxis]) {
          ROS_WARN_STREAM("Stopping controller "
                          << c.name << " for Z axis, idling both axes");
          zaxis_control_mode_ = Control_Idle;
        }
      }
    }
  }

  for (auto const &c : start_list) {
    ROS_WARN_STREAM("+++ Starting controller " << c.name << " : " << c.type);

    for (auto const &r : c.claimed_resources) {
      ROS_WARN_STREAM("+++   Hardware resources : " << r.hardware_interface);
      for (auto const &resource : r.resources) {
        ROS_WARN_STREAM("+++     Resource : " << resource);

        if (resource == JointNames[Yaw]) {
          if (c.type.find("velocity_controllers") == 0) {
            ROS_WARN("Setting yaw to velocity control");
            yaw_control_mode_ = Control_Velocity;
          } else {
            yaw_control_mode_ = Control_Idle;
          }

        } else if (resource == JointNames[CoreXyX] ||
                   resource == JointNames[CoreXyY]) {
          if (c.type.find("velocity_controllers") == 0) {
            ROS_WARN("Setting core XY to velocity control");
            coreXY_control_mode_ = Control_Velocity;
          } else {
            // Control is set, but leave motor idle until a command is
            // published to the motor
            coreXY_control_mode_ = Control_Idle;
          }
        } else if (resource == JointNames[ZAxis]) {
          if (c.type.find("velocity_controllers") == 0) {
            ROS_WARN("Setting z axis to velocity control");
            zaxis_control_mode_ = Control_Velocity;
          } else {
            zaxis_control_mode_ = Control_Idle;
          }
        }
      }
    }
  }
}

bool Testbed::enableSrvCallback(testbed_msgs::AxisEnableRequest &req,
                                testbed_msgs::AxisEnableResponse &res) {
  if (req.enable) {
    ROS_WARN("!!! Enabling all axes");
    if (_yaw) {
      _yaw->enable();
    }
    if (_coreXyA && _coreXyB) {
      _coreXyA->enable();
      _coreXyB->enable();
    }
    if (_zAxis) {
      _zAxis->enable();
    }
  } else {
    ROS_WARN("!!! Disabling all axes");
    if (_yaw) {
      _yaw->disable();
    }
    if (_coreXyA && _coreXyB) {
      _coreXyA->disable();
      _coreXyB->disable();
    }
    if (_zAxis) {
      _zAxis->disable();
    }
  }

  return true;
}

}  // namespace testbed_driver
