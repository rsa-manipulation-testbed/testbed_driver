from setuptools import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    packages=["testbed_cli"],
    install_requires=[
        "Click",
    ],
    scripts=["tbc"],
    # entry_points={
    #     'console_scripts': [
    #         'tbc = testbed_clis:cli',
    #     ],
    # },
)

setup(**d)
