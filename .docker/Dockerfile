ARG ROS_DISTRO=noetic
FROM ros:$ROS_DISTRO-ros-base AS ci

ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /root/testbed_ws
COPY . src/testbed_hw

# Install apt packages needed for CI
RUN apt-get -q update \
    && apt-get -q -y upgrade \
    && apt-get -q install --no-install-recommends -y \
        git \
        lsb-release \
        python3-catkin-tools \
        python3-dev \
        python3-pip \
        python3-vcstool \
        python3-venv \
        ros-${ROS_DISTRO}-catkin \
        software-properties-common \
        sudo \
        wget \
    && rosdep update \
    && vcs import src < src/testbed_hw/testbed_hw.rosinstall \
    && rosdep install -y --from-paths src --ignore-src --rosdistro ${ROS_DISTRO} --as-root=apt:false \
    && rm -rf src \
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

RUN rm -r /root/testbed_ws

FROM ci AS robot
ENV DEBIAN_FRONTEND=noninteractive

ARG USERNAME=ubuntu
ARG USER_UID=1000
ARG USER_GID=$USER_UID

RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME \
    && usermod -a -G dialout $USERNAME \
    && echo "source /usr/share/bash-completion/completions/git" >> /home/$USERNAME/.bashrc

# Switch to the non-root user for the rest of the installation
USER $USERNAME
ENV USER=$USERNAME

# Python in Ubuntu is now marked as a "Externally managed environment",
# Per best practice, create a venv for local python packages
#
# These two ENVs effectively "activate" the venv for subsequent calls to
# python/pip in the Dockerfile
WORKDIR /home/$USERNAME
ENV VIRTUAL_ENV=/home/$USERNAME/.venv/blue
RUN python3 -m venv --system-site-packages --symlinks $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

ENV USER_WORKSPACE=/home/$USERNAME/testbed_ws
WORKDIR $USER_WORKSPACE
COPY --chown=$USER_UID:$USER_GID . src/testbed_hw

WORKDIR $USER_WORKSPACE
RUN sudo apt-get -q update \
    && sudo apt-get -q -y upgrade \
    && vcs import src < src/testbed_hw/testbed_hw.rosinstall \
    && rosdep update \
    && rosdep install -y --from-paths src --ignore-src --rosdistro ${ROS_DISTRO} \
    && sudo apt-get autoremove -y \
    && sudo apt-get clean -y \
    && sudo rm -rf /var/lib/apt/lists/*

# Actually build workspace
RUN . "/opt/ros/${ROS_DISTRO}/setup.sh" \
    && catkin build

RUN echo "source ${USER_WORKSPACE}/devel/setup.bash" >> /home/$USERNAME/.bashrc \
    && echo "source /opt/ros/${ROS_DISTRO}/setup.bash" >> /home/$USERNAME/.bashrc \
    && echo "source $VIRTUAL_ENV/bin/activate" >> /home/$USERNAME/.bashrc
