cmake_minimum_required(VERSION 3.0.2)
project(testbed_teleop)

find_package(catkin REQUIRED COMPONENTS
  controller_manager
  dynamic_reconfigure
  testbed_msgs
  geometry_msgs
  std_msgs
)

catkin_python_setup()

# Generate dynamic reconfigure parameters in the 'cfg' folder
generate_dynamic_reconfigure_options(
  cfg/TestbedTeleop.cfg
)

catkin_package(
  CATKIN_DEPENDS
)

#############
## Install ##
#############

# install(TARGETS ${PROJECT_NAME}
#   ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   RUNTIME DESTINATION ${CATKIN_GLOBAL_BIN_DESTINATION}
# )

install(DIRECTORY launch/
    DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/launch
)
