// A minimalist RobotHW for a single Clearpath motor.
// For development and testing, not for production use
// on testbed.
//
// Copyright 2021 University of Washington
//

#include "clearpath_robot_hw.h"

#include <ros/console.h>

#include <string>

#include "joint_limits_interface/joint_limits_rosparam.h"

namespace clearpath_sc_ros {

using hardware_interface::ControllerInfo;

const float ClearpathRobotHW::DiagnosticUpdateTimerPeriod = 0.1;
const double ClearpathRobotHW::ControlLoopTimerPeriod = 0.1;

const std::array<std::string, ClearpathRobotHW::NumJoints>
    ClearpathRobotHW::JointNames = {"motor"};

ClearpathRobotHW::ClearpathRobotHW()
    : manager_(), control_mode_(Control_Idle) {}

ClearpathRobotHW::~ClearpathRobotHW() {
  if (motor_) motor_->disable();
}

bool ClearpathRobotHW::init(ros::NodeHandle &nh, ros::NodeHandle &pnh) {
  // _testbedStatePub = nh.advertise<testbed_msgs::TestbedState>("state", 10);

  diangostic_updater_.setHardwareID("clearpath");
  nh.createTimer(
      ros::Duration(ClearpathRobotHW::DiagnosticUpdateTimerPeriod),
      [&](const ros::TimerEvent &event) { diangostic_updater_.update(); });

  ROS_WARN("Starting init");

  int sn = -1;
  pnh.getParam("sn", sn);

  if (sn < 0) {
    ROS_FATAL("Motor serial number must be specified in parameter \"sn\"");
    return false;
  }

  try {
    manager_.initialize();

    ROS_INFO_STREAM("Found " << manager_.axes().size() << " nodes on "
                             << manager_.numPorts() << " ports");

    for (auto const &axis : manager_.axes()) {
      IInfo &info(axis->getInfo());

      ROS_INFO_STREAM("  " << info.Model.Value() << " : s/n "
                           << info.SerialNumber.Value() << " : f/w "
                           << info.FirmwareVersion.Value());

      if (int(info.SerialNumber.Value()) == sn) {
        std::cout << "   Found motor s/n " << sn << std::endl;
        motor_ = std::make_shared<ClearpathMotorHw>(JointNames[0], pnh, axis);
      }
    }

  } catch (mnErr &theErr) {
    ROS_ERROR("Caught error");
    printf("Caught error: addr=%d, err=0x%08x\nmsg=%s\n", theErr.TheAddr,
           theErr.ErrorCode, theErr.ErrorMsg);

    return false;
  }

  if (!motor_) {
    ROS_FATAL_STREAM("Unable to find motor with s/n \"" << sn << "\"");
    return false;
  }

  for (int i = 0; i < NumJoints; i++) {
    const std::string jointName = JointNames[i];

    // -- connect and register the ros_control joint state interface --
    hardware_interface::JointStateHandle stateHandle(
        jointName, &joint_position_.data()[i], &joint_velocity_.data()[i],
        &joint_current_.data()[i]);
    joint_state_interface_.registerHandle(stateHandle);

    // connect and register the joint position interface
    hardware_interface::JointHandle posHandle(
        joint_state_interface_.getHandle(jointName),
        &joint_position_command_.data()[i]);
    joint_position_interface_.registerHandle(posHandle);

    hardware_interface::JointHandle velHandle(
        joint_state_interface_.getHandle(jointName),
        &joint_velocity_command_.data()[i]);
    joint_velocity_interface_.registerHandle(velHandle);

    // This could be done in a much more sophisticated way ...
    // see the ros_control boilerplate:
    //
    //  https://github.com/PickNikRobotics/ros_control_boilerplate/blob/0f9cd01bc1430c718554a3801683ada9cd2aa0a6/src/generic_hw_interface.cpp#L119

    // Load joint limits from ros param
    if (!joint_limits_interface::getJointLimits(jointName, nh, limits_)) {
      ROS_WARN_STREAM("Unable to find joint limits for " << jointName);
      continue;
    }

    // A handle used to enforce velocity and acceleration limits of a
    // velocity-controlled joint.
    joint_limits_interface::VelocityJointSaturationHandle
        vel_joint_limits_handle(velHandle, limits_);
    vel_joint_limits_.registerHandle(vel_joint_limits_handle);
  }

  registerInterface(&joint_state_interface_);
  registerInterface(&joint_position_interface_);
  registerInterface(&joint_velocity_interface_);

  _enableAllServer = nh.advertiseService(
      "enable_all", &ClearpathRobotHW::enableSrvCallback, this);

  // Create the controller manager
  controller_manager_.reset(
      new controller_manager::ControllerManager(this, nh));

  control_loop_timer_ = nh.createTimer(ros::Duration(ControlLoopTimerPeriod),
                                       &ClearpathRobotHW::update, this);

  ROS_INFO("Nodelet initialized!");

  return true;
}

void ClearpathRobotHW::update(const ros::TimerEvent &e) {
  elapsed_time_ = ros::Duration(e.current_real - e.last_real);

  read(ros::Time::now(), elapsed_time_);
  controller_manager_->update(ros::Time::now(), elapsed_time_);
  write(ros::Time::now(), elapsed_time_);
}

void ClearpathRobotHW::read(const ros::Time &current_time,
                            const ros::Duration &elapsed_time) {
  // Update motor, and wait for a response
  if (!motor_) return;

  const testbed_driver::MotorState state = motor_->state();
  joint_position_[Clearpath] = state.position.raw();
  joint_velocity_[Clearpath] = state.velocity.raw();
  joint_current_[Clearpath] = state.current.raw();
}

void ClearpathRobotHW::write(const ros::Time &current_time,
                             const ros::Duration &elapsed_time) {
  if (!motor_) return;

  if (motor_->isEnabled()) {
    if (control_mode_ == Control_Position) {
      ROS_DEBUG_STREAM("Setting Clearpath position to "
                       << joint_position_command_[Clearpath] << " rad");
      motor_->setPosition(joint_position_command_[Clearpath]);
    } else if (control_mode_ == Control_Velocity) {
      vel_joint_limits_.enforceLimits(elapsed_time);

      ROS_DEBUG_STREAM("Setting Clearpath velocity to "
                       << joint_velocity_command_[Clearpath] << " rad/sec");
      motor_->setVelocity(joint_velocity_command_[Clearpath]);
    } else {
      motor_->stop();
    }
  }
}

void ClearpathRobotHW::doSwitch(const std::list<ControllerInfo> &start_list,
                                const std::list<ControllerInfo> &stop_list) {
  ROS_WARN_STREAM("In ClearpathRobotHW::doSwitch, starting "
                  << start_list.size() << ", stopping " << stop_list.size());

  for (auto const &c : stop_list) {
    ROS_WARN_STREAM("--- Stopping controller " << c.name << " : " << c.type);

    for (auto const &r : c.claimed_resources) {
      ROS_WARN_STREAM("---   Hardware resources : " << r.hardware_interface);

      for (auto const &resource : r.resources) {
        ROS_WARN_STREAM("---     Resource : " << resource);

        if (resource == JointNames[Clearpath]) {
          ROS_WARN_STREAM("Stopping controller "
                          << c.name << " for Clearpath, idling Clearpath");
          control_mode_ = Control_Idle;
        }
      }
    }
  }

  for (auto const &c : start_list) {
    ROS_WARN_STREAM("+++ Starting controller " << c.name << " : " << c.type);

    for (auto const &r : c.claimed_resources) {
      ROS_WARN_STREAM("+++   Hardware resources : " << r.hardware_interface);

      for (auto const &resource : r.resources) {
        ROS_WARN_STREAM("+++     Resource : " << resource);

        if (resource == JointNames[Clearpath]) {
          ROS_WARN_STREAM("Changing mode for joint \"" << resource << "\"");

          // For now controller name must be at start of controller name
          if (c.type.find("velocity_controllers") == 0) {
            ROS_WARN("Setting motor to velocity control");
            control_mode_ = Control_Velocity;
          } else if (c.type.find("position_controllers") == 0) {
            ROS_WARN("Setting motor to position control");
            control_mode_ = Control_Position;
          } else {
            ROS_WARN_STREAM("Didn't understand controller \"" << c.type
                                                              << "\"");
            control_mode_ = Control_Idle;
          }
        }
      }
    }
  }
}

bool ClearpathRobotHW::enableSrvCallback(
    testbed_msgs::AxisEnableRequest &req,
    testbed_msgs::AxisEnableResponse &res) {
  if (motor_) {
    if (req.enable) {
      if (!motor_->enable()) {
        ROS_WARN("Unable to enable the motor");
      }
    } else {
      if (!motor_->disable()) {
        ROS_WARN("Unable to disable the motor");
      }
    }
  }

  return true;
};

}  // namespace clearpath_sc_ros
