// A minimalist RobotHW for a single Clearpath motor.
// For development and testing, not for production use
// on testbed.
//
// Copyright 2021 University of Washington
//

#pragma once

#include <controller_manager/controller_manager.h>
#include <diagnostic_updater/publisher.h>
#include <hardware_interface/controller_info.h>
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/robot_hw.h>
#include <joint_limits_interface/joint_limits.h>
#include <joint_limits_interface/joint_limits_interface.h>

#include <array>
#include <string>

#include "motor_hw_base/motor_hw_base.h"

// msgs and srvs
#include "clearpath_sc_ros/clearpath_motor_hw.h"
#include "clearpath_sc_ros/manager.h"
#include "testbed_msgs/AxisEnable.h"

namespace clearpath_sc_ros {

class ClearpathRobotHW : public hardware_interface::RobotHW {
 public:
  static const float DiagnosticUpdateTimerPeriod;
  static const double ControlLoopTimerPeriod;

  enum Joint_t { Clearpath = 0, NumJoints = 1 };

  enum JointControlMode {
    Control_Idle = 0,
    Control_Velocity = 1,
    Control_Position = 2
  };

  static const std::array<std::string, NumJoints> JointNames;

  ClearpathRobotHW();
  ~ClearpathRobotHW();

  virtual bool init(ros::NodeHandle &, ros::NodeHandle &);

  virtual void read(const ros::Time &, const ros::Duration &);
  virtual void write(const ros::Time &, const ros::Duration &);

  // Called when controllers switch
  virtual void doSwitch(
      const std::list<hardware_interface::ControllerInfo> & /*start_list*/,
      const std::list<hardware_interface::ControllerInfo> & /*stop_list*/);

 private:
  bool enableSrvCallback(testbed_msgs::AxisEnableRequest &,
                         testbed_msgs::AxisEnableResponse &);
  ros::ServiceServer _enableAllServer;

  clearpath_sc_ros::Manager manager_;
  ClearpathMotorHw::Ptr motor_;

  JointControlMode control_mode_;

  ros::Timer diangostic_update_timer_;
  diagnostic_updater::Updater diangostic_updater_;

  // Current joint states
  std::array<double, NumJoints> joint_position_;
  std::array<double, NumJoints> joint_velocity_;
  std::array<double, NumJoints> joint_current_;

  // Commands to the joints
  std::array<double, NumJoints> joint_position_command_;
  std::array<double, NumJoints> joint_velocity_command_;

  // Interface for publishing joint state out
  hardware_interface::JointStateInterface joint_state_interface_;

  // Interfaces for joint position and velocity commands in
  hardware_interface::PositionJointInterface joint_position_interface_;
  hardware_interface::VelocityJointInterface joint_velocity_interface_;

  // Joint limits loaded from ros param
  joint_limits_interface::JointLimits limits_;

  // Interface for enforcing limits on a velocity-controlled joint through
  // saturation
  joint_limits_interface::VelocityJointSaturationInterface vel_joint_limits_;

  // Control loop
  void update(const ros::TimerEvent &e);
  ros::Timer control_loop_timer_;
  ros::Duration elapsed_time_;

  boost::shared_ptr<controller_manager::ControllerManager> controller_manager_;
};

}  // namespace clearpath_sc_ros
