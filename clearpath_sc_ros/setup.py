from setuptools import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    packages=["clearpath_sc_ros"],
    scripts=["nodes_py/clearpath_teleop"],
)

setup(**d)
