#include "clearpath_sc_ros/manager.h"

#include <iostream>

using namespace sFnd;

namespace clearpath_sc_ros {

Manager::Manager() : _mgr(SysManager::Instance()) {}

Manager::~Manager() {
  // Axes try to shut themselves down during destruction
  // So manager must be running when they are destroyed
  _axes.clear();

  // If not done previously
  _mgr->PortsClose();
}

void Manager::initialize() {
  std::vector<std::string> portNames;
  SysManager::FindComHubPorts(portNames);

  // Editing of the list of portnames (e.g. ignoring particular
  // ports) could happen here.

  _numPorts = portNames.size();
  for (size_t portNum = 0;
       portNum < portNames.size() && portNum < NET_CONTROLLER_MAX; portNum++) {
    _mgr->ComHubPort(portNum, portNames[portNum].c_str());
  }

  _mgr->PortsOpen(_numPorts);

  for (size_t portNum = 0; portNum < _numPorts; ++portNum) {
    IPort &port = _mgr->Ports(portNum);

    for (size_t nodeNum = 0; nodeNum < port.NodeCount(); nodeNum++) {
      INode &node = port.Nodes(nodeNum);

      // Make sure we are talking to a ClearPath SC (advanced or basic model)
      if (node.Info.NodeType() != IInfo::CLEARPATH_SC_ADV &&
          node.Info.NodeType() != IInfo::CLEARPATH_SC) {
        //("---> ERROR: Uh-oh! Node %d is not a ClearPath-SC Motor\n", iNode);
        continue;
      }

      _axes.push_back(std::make_shared<ClearpathMotor>(node));
    }
  }
}

void Manager::getShutdownInfo() {
  for (size_t portNum = 0; portNum < _numPorts; ++portNum) {
    IPort &port = _mgr->Ports(portNum);

    for (size_t nodeNum = 0; nodeNum < port.NodeCount(); nodeNum++) {
      INode &node = port.Nodes(nodeNum);
      IInfo &info(node.Info);

      ShutdownInfo shutdownInfo;
      port.GrpShutdown.ShutdownWhenGet(nodeNum, shutdownInfo);

      char state[256];
      shutdownInfo.statusMask.cpm.StateStr(state, 255);
      std::cout << "Port " << nodeNum << ": S/N " << info.SerialNumber.Value()
                << " Group shutdown: "
                << (shutdownInfo.enabled ? "ENABLED" : "DISABLED") << ": "
                << state << std::endl;
    }
  }
}

void Manager::setGlobalShutdown(bool enabled) {
  for (size_t portNum = 0; portNum < _numPorts; ++portNum) {
    IPort &port = _mgr->Ports(portNum);

    for (size_t nodeNum = 0; nodeNum < port.NodeCount(); nodeNum++) {
      INode &node = port.Nodes(nodeNum);
      ShutdownInfo shutdownInfo;
      port.GrpShutdown.ShutdownWhenGet(nodeNum, shutdownInfo);

      shutdownInfo.enabled = enabled;
      port.GrpShutdown.ShutdownWhen(nodeNum, shutdownInfo);
    }
  }
}

}  // namespace clearpath_sc_ros
