#!/usr/bin/env python3

import rospy
from enum import Enum

from sensor_msgs.msg import Joy
from std_msgs.msg import Float64

import testbed_teleop


class ControlMode(Enum):
    VELOCITY = 1
    POSITION = 2


namespace = testbed_teleop.Namespace("/clearpath")
controllers = testbed_teleop.ControllerManager(namespace)
enable_service = testbed_teleop.EnableService(namespace)


class AxisControl:
    def __init__(self, ns, name, controller_name, gain):
        self.ns = ns
        self.name = name
        self.gain = gain

        command_topic = self.ns / controller_name / "command"
        self.pub = rospy.Publisher(str(command_topic), Float64, queue_size=10)

        controllers.switch_controllers(self.name, str(self.ns / controller_name))

    def process_joy(self, joy):
        out = joy.x() * self.gain

        self.pub.publish(out)


class ClearpathTeleop:
    def __init__(self):
        rospy.Subscriber("joy", Joy, self.on_joy_callback)
        self.joy = testbed_teleop.LogitechJoystickMap()

        self.control = None

        self.velocity_gain = 100
        self.position_gain = 30

    def velocity_control(self):
        self.mode = ControlMode.VELOCITY
        self.control = AxisControl(
            namespace, "motor", "velocity_controller", self.velocity_gain
        )

    def position_control(self):
        self.mode = ControlMode.POSITION
        self.control = AxisControl(
            namespace, "motor", "position_controller", self.position_gain
        )

    def on_joy_callback(self, joy):
        self.joy.update(joy)

        if self.joy.enable():
            enable_service.srv(enable=True)

            if self.control is None:
                self.velocity_control()

        elif self.joy.disable():
            enable_service.srv(enable=False)

        elif self.joy.switch_mode():
            rospy.logwarn("Switching motor mode")
            if self.mode == ControlMode.VELOCITY:
                self.position_control()
            else:
                self.velocity_control()

        if self.control:
            self.control.process_joy(self.joy)


if __name__ == "__main__":
    rospy.init_node("clearpath_teleop")
    node = ClearpathTeleop()
    rospy.spin()
