// Copyright 2021 University of Washington

#include <chrono>
#include <string>
using namespace std::chrono_literals;

#include "motor_hw_base/motor_hw_base.h"

namespace testbed_driver {

MotorHwBase::MotorHwBase(const std::string &name,
                         const ros::NodeHandle &parentnh)
    : _name(name) {
  ros::NodeHandle nh(parentnh, name);
  _axisEnableServer =
      nh.advertiseService("enable", &MotorHwBase::axisEnableSrvCallback, this);
}

MotorHwBase::~MotorHwBase() {}

bool MotorHwBase::axisEnableSrvCallback(testbed_msgs::AxisEnableRequest &req,
                                        testbed_msgs::AxisEnableResponse &res) {
  if (req.enable) {
    enable();
  } else {
    disable();
  }
  return true;
}

}  // namespace testbed_driver
