// Copyright 2021 University of Washington
//
//

#include <chrono>
#include <string>
using namespace std::chrono_literals;

#include "motor_hw_base/sim_motor_hw.h"

namespace testbed_driver {

const float SimMotorHw::ControlLoopPeriod = 0.01;

SimMotorHw::SimMotorHw(const std::string &name, const ros::NodeHandle &parentnh)
    : MotorHwBase(name, parentnh),
      mode_(SIM_DISABLED),
      cmd_velocity_(0.0),
      cmd_position_(0.0),
      velocity_(0.0),
      position_(0.0) {
  ros::NodeHandle nh(parentnh, name);

  _controlLoopTimer = nh.createTimer(
      ros::Duration(SimMotorHw::ControlLoopPeriod), &SimMotorHw::update, this);
}

SimMotorHw::~SimMotorHw() {}

MotorState SimMotorHw::state() const {
  MotorState state;

  ROS_INFO_STREAM_THROTTLE(1.0, "State: " << modeToString(mode_));

  const float SimEncoderResolution = 1e9;

  state.position = EncoderValue(position_ * SimEncoderResolution,
                                1.0 / SimEncoderResolution);
  state.velocity = EncoderValue(velocity_ * SimEncoderResolution,
                                1.0 / SimEncoderResolution);
  state.current = EncoderValue(0);

  return state;
}

std::string SimMotorHw::modeToString(SimMode_t mode) const {
  enum SimMode_t { SIM_DISABLED, SIM_IDLE, SIM_POSITION, SIM_VELOCITY };

  if (mode == SimMotorHw::SIM_DISABLED)
    return "DISABLED";
  else if (mode == SimMotorHw::SIM_IDLE)
    return "IDLE";
  else if (mode == SimMotorHw::SIM_POSITION)
    return "POSITION";
  else if (mode == SimMotorHw::SIM_VELOCITY)
    return "VELOCITY";

  return "(unknown)";
}

void SimMotorHw::update(const ros::TimerEvent &e) {
  const auto dt = e.current_real - e.last_real;

  auto p = position_ + velocity_ * dt.toSec();
  position_ = p;
}

}  // namespace testbed_driver
